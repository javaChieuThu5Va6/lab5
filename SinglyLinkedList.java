package lab5;

/**
 *
 * @author xuanhatlu@gmaii.com
 */
public class SinglyLinkedList {

    protected Node head;
    protected Node last;

    public Node getHead() {
        return head;
    }

    public void setHead(Node head) {
        this.head = head;
    }

    public Node getLast() {
        return last;
    }

    public void setLast(Node last) {
        this.last = last;
    }

    public SinglyLinkedList() {
        head = null;
        last = null;
    }

    public void insertFirst(String item) {
        Node newNode = new Node(item);
        if (head == null) {
            head = newNode;
            last = newNode;
            System.out.println("insert first: head=null");
        } else {
            Node tempHead = head;
            newNode.setNext(tempHead);
            head = newNode;
            System.out.println("head item isnert first: " + head.getItem());
        }
        print();
    }

    public void insertLast(String item) {
        Node newNode = new Node(item);
        if (head == null) {
            head = newNode;
            last = newNode;
            System.out.println("insert last: head ==null");
        } else {
            Node tempLast = last;
            tempLast.setNext(newNode);
            last = tempLast;
            System.out.println("last item isnert last: " + head.getItem());

        }
        print();
    }

    public void insertAfter(Node n, String s) {
        String itemResult = n.getItem();
        Node cur = head;
        while (cur != null) {
            if (cur.getItem().equals(itemResult)) {
                Node newNode = new Node(s);
                Node temp = cur.getNext();
                if (cur == last) {
                    insertLast(s);
                } else {

                    cur.setNext(newNode);
                    newNode.setNext(temp);
                }
                System.out.println("insertAfter ; item data truoc: " + cur.getItem()
                        + " item curr(item truyen vao)" + newNode.getItem()
                        + " item tiep theo " + temp.getItem());
                print();
                return;
            }
            cur = cur.getNext();
        }
        System.out.println("khong tim thay node n");
        return;
    }

    public void insertBefore(Node n, String s) {
        String itemResult = n.getItem();
        Node cur = head;
        Node beforeCurr = head;
        while (cur != null) {
            if (cur.getItem().equals(itemResult)) {
                Node newNode = new Node(s);
                if (cur == head) {
                    insertFirst(s);
                } else {

                    newNode.setNext(cur);

                    beforeCurr.setNext(newNode);
                }
                System.out.println("inser before ; item data truoc: " + beforeCurr.getItem()
                        + " item curr(item truyen vao) " + newNode.getItem()
                        + " item tiep theo " + cur.getItem());
                print();
                return;
            }
            beforeCurr = cur;
            cur = cur.getNext();
        }
        System.out.println("khong tim thay node n");

        return;
    }

    public void remove(Node n) {
        Node cur = head;
        Node beforeCur = head;
        String itemResult = n.getItem();
        while (cur != null) {
            if (cur.getItem().equals(itemResult)) {
                Node temp = cur.getNext();
                beforeCur.setNext(temp);
                System.out.println("sau khi xoa: item vi tri turoc(before ): " + beforeCur.getItem()
                        + "item vi tri sau(cur.getnext()): " + temp.getItem());
                print();
                return;
            }
            beforeCur = cur;
            cur = cur.getNext();
        }
        System.out.println("khong tim thay node n");
        return;
    }

    public Node first() {
        return head;
    }

    public Node last() {
        return last;
    }

    public Node next(Node n) {
        Node cur = head;
        while (cur != null) {
            if (cur.getItem().equals(n.getItem())) {
                return cur.getNext();
            }
            cur = cur.getNext();
        }
        return null;
    }

    public Node prev(Node n) {
        Node cur = head;
        Node beforeCur = head;
        while (cur != null) {
            if (cur.getItem().equals(n.getItem())) {
                return beforeCur;
            }
            beforeCur = cur;
            cur = cur.getNext();
        }
        return null;
    }

    public void print() {
        Node cur = head;
        System.out.println("chuoi item: ");
        while (cur != null) {
            System.out.print(" " + cur.getItem().toString());
            cur = cur.getNext();
        }
        System.out.println("\n");
    }

    public Node removeLast() {
        Node tempLast = last;
        Node cur = head;
        Node preCur = head;
        if (cur == null) {
            System.out.println("removeLast(pop rong, dequeu rong): khong co gi de lay ra");
            return null;
        } else {
            while (cur.getNext() != null) {
                System.out.print(" " + cur.getItem().toString());
                preCur = cur;
                cur = cur.getNext();
            }
            preCur.setNext(null);
            print();
            return tempLast;
        }
    }
}